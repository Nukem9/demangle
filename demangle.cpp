#include "stdafx.h"

extern "C"
{
#ifndef _WIN64
#define SYM_IMP_NAME "__imp__"
#else
#define SYM_IMP_NAME "__imp_"
#endif // ndef _WIN64

	char *__unDName(char* buffer, const char* mangled, int buflen, malloc_func_t memget, free_func_t memfree, unsigned int flags);
}

BOOL UndecorateSymbolEx(char *Buffer, size_t BufferLen, const char *Symbol, int Flags)
{
	char *demangled = __unDName(Buffer, Symbol, BufferLen, malloc, free, (Flags & 0x0FFFFFFF));

	// Succeeded
	if (strcmp(demangled, Symbol))
		return TRUE;

	// Check to undecorate symbols with imports
	if (Flags & UNDNAME_SPECIAL_NOIMP)
	{
		if (!_strnicmp(Symbol, SYM_IMP_NAME, strlen(SYM_IMP_NAME)))
		{
			Symbol += strlen(SYM_IMP_NAME);

#ifndef _WIN64
			// Sometimes there is still a trailing '@##'
			goto __striparg;
#else
			strcpy_s(Buffer, BufferLen, Symbol);
			return TRUE;
#endif // ndef _WIN64
		}
	}

#ifndef _WIN64
	// Check to undecorate C-style symbols
	//
	// http://msdn.microsoft.com/en-us/library/x7kb4e2f(VS.80).aspx
	// __cdecl (the default) Leading underscore (_)
	// __stdcall             Leading underscore (_) and a trailing at sign (@) followed by a number representing the number of bytes in the parameter list
	// __fastcall            Same as __stdcall, but prepended by an at sign instead of an underscore
	if (Flags & UNDNAME_SPECIAL_C)
	{
		// Does it begin with '_' or '@'?
		if (Symbol[0] == '_' || Symbol[0] == '@')
			Symbol++;
		else
			return FALSE;

__striparg:
		// Base copy length
		size_t copyLen = 0;

		// Chop off the trailing '@'
		{
			const char *temp = Symbol;
			const char *last = nullptr;

			for (; *temp; temp++)
			{
				if (*temp == '@')
					last = temp;
			}

			temp	= min(temp, last);
			copyLen = min(BufferLen, (size_t)(temp - Symbol));
		}

		strncpy_s(Buffer, BufferLen, Symbol, copyLen);
		return TRUE;
	}
#endif // ndef _WIN64

	return FALSE;
}

BOOL UndecorateSymbol(char *Buffer, size_t BufferLen, const char *Symbol)
{
	return UndecorateSymbolEx(Buffer, BufferLen, Symbol, UNDNAME_COMPLETE | UNDNAME_SPECIAL_NOIMP | UNDNAME_SPECIAL_C);
}

void DemangleSymbol(const char *Symbol)
{
	char buf[256];
	memset(buf, 0, sizeof(buf));

	UndecorateSymbol(buf, sizeof(buf), Symbol);

	printf("%s\n", buf);
}

int main(int argc, char **argv)
{
	DemangleSymbol("?SHPathCreateFromUrl@@YGJPBGPAVShStrW@@K@Z");
	DemangleSymbol("__imp_NtQueryInformationProcess");// x64
	DemangleSymbol("__imp__NtQueryInformationProcess@20");
	DemangleSymbol("__imp__NtQueryInformationProcess");
	DemangleSymbol("_NtQueryInformationProcess");
	DemangleSymbol("_NtQueryInformationProcess@20");
	DemangleSymbol("@NtQueryInformationProcess@20");
	DemangleSymbol("NtQueryInformationProcess");

	getchar();
	return 0;
}

